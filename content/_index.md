---
title: " "
decription: "Das ist die Homepage des Linux-macht-Schule-Projekts"
featured_image: '/images/default.jpg'
---

# Willkommen auf der Linux-Entdeckungsreise! 

Hier erfährst du spannende Dinge über ein besonderes Computersystem namens Linux.
Bist du bereit, die Welt der Computer zu erkunden?

## Was ist Linux?

Linux ist wie das Gehirn eines Computers.
Es ist ein Betriebssystem, das es Computern ermöglicht zu denken und Aufgaben zu erledigen.

![tux](/images/tux.gif)

### Das ist Tux -- der Linux-Pinguin! 

Er liebt es, mit Freunden zu programmieren und Probleme zu lösen. 
Vielleicht hast du ja Lust, mit Tux die Welt der Computer zu erkunden!


## Warum ist Linux wichtig?

Linux ist überall!
Es arbeitet in Servern, Handys und sogar in einigen deiner Lieblings-Spielkonsolen.
Es hilft, Dinge zu organisieren und hält die digitale Welt am Laufen.

