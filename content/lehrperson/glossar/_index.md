---
title: "/glossar"
featured_image: '/images/default.jpg'
---

{{< toc >}}

### Distribution

In Bezug auf Linux bezieht sich der Begriff "Distribution" (oft als "Distro" abgekürzt) auf eine vollständige Betriebssystemversion, die auf dem Linux-Kernel basiert.
Eine Linux-Distribution besteht nicht nur aus dem Kernel selbst, sondern auch aus einer Sammlung von Systemwerkzeugen, Bibliotheken, Anwendungen und oft einem Installationsprogramm, das es Benutzern ermöglicht, das Betriebssystem auf ihren Computern zu installieren.

Es gibt viele verschiedene Linux-Distributionen, die für unterschiedliche Zwecke und Zielgruppen entwickelt wurden.
Einige sind auf Benutzerfreundlichkeit für Desktop-Systeme ausgerichtet, während andere für Server, eingebettete Systeme oder andere spezielle Anforderungen entwickelt wurden.

### Desktop-Umgebung

Eine Desktop-Umgebung ist eine Software-Suite, die das Arbeiten am Computer erleichtert.
Sie liefert eine grafische Benutzeroberfläche mit Fenstern, Symbolen und Menüs.
Bekannte Beispiele sind GNOME, KDE oder Xfce.
Diese Programme machen es einfacher, Programme zu starten, Dateien zu verwalten und den Computer zu nutzen, ohne viel technisches Wissen haben zu müssen.
Insgesamt sorgen sie für eine ansprechende und benutzerfreundliche Darstellung des Computers.

### Kernel

Der Kernel ist das Herzstück eines Betriebssystems.
Er ist die grundlegende Software, die die Kommunikation zwischen der Hardware (wie Prozessor, Speicher und Peripheriegeräten) und der Software (den Anwendungen, die Sie auf Ihrem Computer ausführen) ermöglicht.

Der Linux-Kernel wurde von Linus Torvalds geschrieben und bildet die Basis vieler Betriebssysteme, die unter dem Begriff "Linux" bekannt sind.

### Linus Torvalds

Linus Torvalds ist Gründer des freien Software-Projekts Linux, das bis heute von ihm koordiniert und entwickelt wird.

Er war schon als junger Mann daran interessiert, an Computern zu entwickeln und programmierte eine Assemblersprache, einen Texteditor und einige Spiele.

Am 25. August 1991 kündigte er als junger Informatikstudent an, dass er ein eigenes Betriebssystem entwickeln wird.
Am 17. September 1991, konnte dann die erste Version dessen heruntergeladen werden.

Die erste fehlerfrei funktionierende Version wurde schlussendlich im März 1994 vorgestellt.

### proprietär

Der Begriff "proprietär" bezieht sich auf Software, deren Quellcode nicht öffentlich verfügbar ist.
Das bedeutet, dass der Code, aus dem die Software erstellt wurde, geheim gehalten wird.

Betriebssysteme wie Windows und macOS sind proprietär, da der Quellcode nicht für die Öffentlichkeit zugänglich ist und nur von den Unternehmen, die sie entwickelt haben, bearbeitet werden kann.

### Unix

Unix ist ein Betriebssystem, das in den 1970er Jahren entwickelt wurde.
Es diente als Grundlage für viele andere Betriebssysteme, darunter auch Linux.

Unix ist bekannt für seine Mehrbenutzerfähigkeit, Multitasking und Portabilität.
Es wird oft in Serverumgebungen eingesetzt.
