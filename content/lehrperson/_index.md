---
title: " "
decription: "Das ist die Homepage des Linux-macht-Schule-Projekts"
featured_image: '/images/default.jpg'
---

# Linux macht Schule

Dieses Projekt richtet sich primär an Lehrpersonen auf der Primarstufe.

Auf dieser Webseite finden sich Informationen, mit welchen der Zugang zur Arbeit mit Linux erleichter wird,
und Unterrichtsmaterialien, mit welchen die Schüler:innen die Welt der Computer und des Programmierens erkunden können.

{{< toc >}}

## Was ist Linux?

Linux ist ein quelloffener, [Unix](/lehrperson/glossar#unix)-ähnlicher [Betriebssystemkernel](/lehrperson/glossar#kernel), mit dessen Entwicklung [Linus Torvalds](/lehrperson/glossar#linus-torvalds) 1991 begann.
Der Kernel bildet das Herzstück eines Betriebssystems und stellt die grundlegenden Funktionen für die Hardware zur Verfügung.
Linux ist bekannt für seine Stabilität, Sicherheit und Flexibilität.

Anders als bei [proprietären](/lehrperson/glossar#proprietär) Betriebssystemen wie Windows oder macOS ist Linux frei verfügbar und kann von allen modifiziert, verbreitet und genutzt werden.
Es basiert auf dem Prinzip der Open-Source-Software, was bedeutet, dass der Quellcode öffentlich zugänglich ist.
Dies hat zu einer Vielzahl von Linux-[Distributionen](/lehrperson/glossar#distribution) geführt, die verschiedene Konfigurationen und Softwarepakete enthalten, um den Bedürfnissen verschiedener Benutzer gerecht zu werden.

## Wo wird Linux angewandt?

Linux wird in vielen verschiedenen Umgebungen eingesetzt, von Desktop-Systemen über Server bis hin zu eingebetteten Systemen.
Aufgrund seiner Stabilität und Sicherheit wird Linux häufig in Serverumgebungen für Web-Hosting, Datenbanken und Netzwerkdienste verwendet.
Es gibt auch viele Desktop-Varianten von Linux wie Ubuntu, Fedora und Debian oder etwas bekannter vielleicht Android und ChromeOS.

Auf dieser [Karte](https://umap.openstreetmap.fr/en/map/lernstick-anwender_10898?scaleControl=false&miniMap=false&scrollWheelZoom=true&zoomControl=true&editMode=disabled&moreControl=true&searchControl=null&tilelayersControl=null&embedControl=null&datalayersControl=true&onLoadPanel=caption&captionBar=false&captionMenus=true) sieht man, wo der Lernstick überall angewandt wird:

{{< rawhtml >}}
<iframe width="100%" height="300px" frameborder="0" allowfullscreen allow="geolocation" src="//umap.openstreetmap.fr/en/map/lernstick-anwender_10898?scaleControl=false&miniMap=false&scrollWheelZoom=false&zoomControl=true&editMode=disabled&moreControl=true&searchControl=null&tilelayersControl=null&embedControl=null&datalayersControl=true&onLoadPanel=caption&captionBar=false&captionMenus=true"></iframe>
{{</ rawhtml >}}

## Warum Linux in der Schule?

### (Kosten)freie Software

Linux ist kostenlos und Open-Source.
Schulen können Geld sparen, indem sie Linux-basierte Systeme verwenden, da keine Lizenzgebühren anfallen.
Zudem könnten Schüler:innen und Lehrer:innen den Quellcode einsehen, was die Möglichkeit eröffnet, die Funktionsweise eines Betriebssystems zu verstehen und zu lehren.

### Sicherheit und Stabilität

Linux gilt als stabiles und sicheres Betriebssystem.
Dies ist besonders wichtig in Bildungseinrichtungen, um einen reibungslosen Betrieb der Systeme zu gewährleisten und die Schüler:innen vor potenziellen Sicherheitsrisiken zu schützen.

### Flexibilität und Vielfalt

Es gibt viele verschiedene Linux-Distributionen, die sich an verschiedene Bedürfnisse anpassen lassen.
Dies ermöglicht Lehrpersonen und Schüler:innen die Auswahl der am besten geeigneten Distribution für ihre Anforderungen.

Es gibt speziell für den Einsatz in der Schule entwickelte Linux-Distributionen wie den [Lernstick](https://lernstick.ch) aus der Schweiz und das etwas bekanntere [Edubuntu](https://edubuntu.org).

### Lernmöglichkeiten

Die Verwendung von Linux bietet Schüler:innen die Möglichkeit, Einblicke in Open-Source-Software und verschiedene Betriebssysteme zu erhalten.
Dies kann dazu beitragen, ihre technischen Fähigkeiten zu verbessern und ein tieferes Verständnis für Computersysteme zu entwickeln.

### Nachhaltigkeit

Linux kann auf älterer Hardware effizient laufen, was dazu beiträgt, die Lebensdauer von vorhandener Hardware zu verlängern und den Bedarf an häufigen Hardware-Upgrades zu reduzieren.

Zudem läuft Linux auch auf Leistungsschwächeren Geräten wie dem Single-Board-Computer [Raspberry Pi](https://raspberrypi.org), mit welchem weitere spannende Projekte gemacht werden können.
