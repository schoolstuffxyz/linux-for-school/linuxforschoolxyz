---
title: "/thesis"
decription: "Das ist die Bachelorarbeit"
featured_image: '/images/default.jpg'
---

# Meine Arbeit

Meine Bachelorarbeit kann hier als PDF gelesen und heruntergeladen werden.

{{< rawhtml >}}
<embed src="./bachelorarbeit.pdf" width="85%" height="150%" 
 type="application/pdf">
{{</ rawhtml >}}

PS: Ich entschuldige mich vielmals bei [Beat Döbeli Honegger](https://beat.doebe.li/) für die falsche Zitation. ;)

