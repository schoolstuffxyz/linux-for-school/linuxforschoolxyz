---
title: "/die-befehlszeile"
menu:
    teacher:
        parent: "/einheiten"
files:
    - ["Lehrpersonenkommentar", "https://gitlab.com/schoolstuff-ch/linux-macht-schule/linux-macht-schule/-/releases/permalink/latest/downloads/release/lehrpersonenkommentar.pdf"]
    - ["die Befehlszeile", "https://gitlab.com/schoolstuff-ch/linux-macht-schule/linux-macht-schule/-/releases/permalink/latest/downloads/release/die-befehlszeile.pdf"]
    - ["Bashcrawl", "https://gitlab.com/schoolstuff-ch/linux-macht-schule/bashcrawl/-/releases"]
    - ["Bashcrawl ausprobieren", "https://mybinder.org/v2/gh/schoolstuff-ch/linux-macht-schule/bashcrawl/HEAD"]
weight: 3
featured_image: '/images/terminal.jpg'
---

{{< toc >}}

## Terminal?

![Terminal - Animiertes Gif](/images/terminal-narrow.gif)

Das Terminal (oder die Kommando- / Befehlszeile) ist ein Programm, mit dem die wenigsten Windows- / MacOS-Anwender bereits in Kontakt gekommen sind.

Es kann als das Gegenstück zur Grafischen Benutzeroberfläche betrachtet werden.
Alte Systeme (70er - 80er Jahre) hatten ausschliesslich ein Terminal und wurden per Text-Befehl bedient.

Im Terminal werden Programme über Befehle angesteuert.
Diese Befehle können auch in einander verschachtelt werden, um mehrer Programme miteinander zu verknüpfen.

Dies bietet den Schüler:innen einen guten Platz, um den Einstieg in das Textbasierte Programmieren zu machen.

## Bashcrawl [Spiel]

Bashcrawl ist ein Abenteuerspiel, das ähnlich den Computerspielen aus den 70er und 80er-Jahren auf Text basiert.
Während dem Spiel werden alte Gemäuer erkundet und mit Zaubersprüchen und Schwertern wird gegen Gegner gekämpft.

Mit dem Spiel können die Schüler:innen (und Lehrer:innen) die Grundlagen der Befehlszeile erlernen.

### Im Browser spielen

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gh/schoolstuff-ch/linux-macht-schule/bashcrawl/HEAD)
Bashcrawl kann dank Binder (über den Button links) im Browser ausprobiert werden. Dann braucht es einen Moment, bis sich der virtuelle Computer aufgestartet hat.
Ist alles bereit, wird das Spiel über den Button "Terminal" automatisch gestartet.

![Terminal starten](/images/launch-binder.png)

Für den Unterricht empfiehlt es sich, das Spiel herunterzuladen.
In der Online-Version können keine Fortschritte gespeichert werden und man muss jedes Mal neu beginnen.

## Unterrichtseinheit

Am Anfang der Unterrichtseinheit wird den Schüler:innen erklärt, was die Befehlszeile ist und wie diese bedient wird.

Daraufhin wird ein Spiel (Bashcrawl) vorgestellt, mit dem die Schüler:innen das Terminal und seine Befehle erkunden können.
Das ist ein Text-Abenteuerspiel, in welchem alte Gemäuer erkundet werden und mit Zaubersprüchen und Schwertern gegen Gegner gekämpft wird.

Zum Schluss wird erläutert und angeleitet, wie die Schüler:innen eigene kleine Skripte / Programme schreiben können.

*Die Unterrichtseinheit "Mein Webserver" nutzt einen Befehl in der Kommandozeile, um eine selbstgeschriebene Webseite im Netzwerk zu veröffentlichen.* 
