---
title: "/installieren"
menu:
    teacher:
        parent: "/einheiten"
files:
    - ["Lehrpersonenkommentar", "https://gitlab.com/schoolstuff-ch/linux-macht-schule/linux-macht-schule/-/releases/permalink/latest/downloads/release/lehrpersonenkommentar.pdf"]
    - ["Linux installieren", "https://gitlab.com/schoolstuff-ch/linux-macht-schule/linux-macht-schule/-/releases/permalink/latest/downloads/release/linux-installieren.pdf"]
    - ["Lernstick EDU", "https://releases.lernstick.ch/lernstick_debian12_latest.iso"]
    - ["Edubuntu", "https://cdimages.ubuntu.com/edubuntu/releases/23.10/release/edubuntu-23.10-desktop-amd64.iso"]
weight: 1
featured_image: '/images/installieren.jpg'
---

{{< toc >}}

Zur Installation von Linux-Distros steht eine Brochure (und ein Lehrpersonenkommentar) zum Download bereit, mit der die Schüler:innen diese auch selbständig vornehmen können.

## Distribution wählen

Für den Einsatz in der Schule wurden im Laufe der Zeit verschiedene Linux-Distributionen entwickelt.
Es wird empfohlen, eine dieser Distros auszuwählen, da sie bereits mit nützlicher Software für den schulischen Gebrauch ausgestattet sind.


Sollten diese Distros nicht Ihren Anforderungen entsprechen, können Sie auf der Seite [DistroWatch](https://distrowatch.org) nach einer anderen passenden Distribution suchen.

### Lernstick

Im Rahmen des Lernstick-Projekts aus der Schweiz wurde eine Distro entwickelt, die darauf ausgelegt ist, portabel auf einem USB-Stick verwendet zu werden.
Der Lernstick bietet eine flexible und einfach einzurichtende Lernumgebung für Schüler:innen.

Das Datenträgerabbild (.iso) für den Lernstick kann [hier](https://www.bfh.ch/de/forschung/forschungsbereiche/lernstick/downloads/) heruntergeladen werden.

Erfahren Sie mehr über den Lernstick auf der offiziellen Webseite [hier](https://lernstick.ch).

*Das Lernstick-Team bietet nach Wunsch einen kostenpflichtigen Service an.
Sie konfigurieren das Betriebssystem nach Wunsch (es können auch viele Windows-Programme eingerichtet werden), helfen bei der Einrichtung und bieten Support für allfällige Fragen und Probleme.*

### Edubuntu

Edubuntu wurde von Lehrpersonen entwickelt und bietet eine stabile, sichere und die Privatsphäre respektierende Lernumgebung für die Schule.
Es basiert auf der bekannten Linux-Distribution Ubuntu und kommt mit einer übersichtlichen und benutzerfreundlichen Oberfläche.

Weitere Informationen und den Download finden Sie auf der offiziellen Webseite [hier](https://edubuntu.org).

## Installation

Nachdem Sie die gewünschte Distro als Datenträgerabbild (.iso) heruntergeladen haben, muss es auf einen USB-Stick oder ein anderes Speichermedium mit ausreichender Kapazität kopiert werden.
Da die Distro das vorhandene Material auf dem Medium ersetzen soll, ist dafür spezielle Software erforderlich.

Für das Kopieren (''Flashen'') hat sich das Programm [BalenaEtcher](https://etcher.balena.io) besonders bewährt.

### Speichermedium auswählen

Je nach Anwendung eignen sich bestimmte Speichermedien besser:

- Für den Betrieb eines [Raspberry Pis](https://raspberrypi.org) wird empfohlen, das .iso auf eine SD-Karte zu flashen.
- Für den Betrieb am PC oder Mac eignen sich USB-Sticks oder natürlich Festplatten (HDD / SSD).

### Datenträgerabbild kopieren

Mit BalenaEtcher können Sie ganz einfach das heruntergeladene .iso auswählen.

![BalenaEtcher Screenshot](/images/balena-0.png)

Anschließend wird das Ziel-Speichermedium ausgewählt.

Achten Sie darauf, dass **nicht** die Festplatte des Computers ausgewählt wird, um Datenverlust zu vermeiden.

![BalenaEtcher Screenshot](/images/balena-1.png)

Und mit einem Klick auf ''Flash'' wird der Prozess gestartet (**Achtung:** das Speichermedium wird komplett überschrieben).

### Finale Installation

Das Dateisystem auf dem so mit dem Datenträgerabbild geflashten USB-Stick ist meist unveränderlich (immutable).
Das heisst, alle Änderungen auf der Systempartition gehen bei einem Neustart verloren.

Deshalb bietet es sich an, das System vom USB-Stick aus auf einen zweiten (oder 20) weiteren Sticks zu installieren.

Dafür wird das Linux vom USB-Stick gestartet und mit der darauf vorhandenen Installationsumgebung wird es auf einen zweiten (oder bspw. eine Festplatte) installiert.

### Linux vom USB-Stick starten

Damit von einem USB-Stick gebootet werden kann, muss der Computer zuerst heruntergefahren werden. ***Info:** Einige Windows-Installationen fahren den Computer nur bei einem Neustart komplett herunter.*

Während dem erneuten Aufstarten wird dann der USB-Stick als Startmedium ausgewählt. Das Bootmenu zur Auswahl:

- erscheint entweder von alleine, wenn der Stick angeschlossen ist.
- muss mit einer [Tastenkombination](https://www.ionas.com/wissen/tastenkombinationen-bios-bootmenue-recovery-partition/) aufgerufen werden.
- kann im [BIOS](https://www.ionas.com/wissen/tastenkombinationen-bios-bootmenue-recovery-partition/) näher konfiguriert werden.

Dieses Video des Lernstick Teams erklärt den Vorgang noch etwas genauer für Macs und PCs:

{{< rawhtml >}}
<iframe width="620" height="349" src="https://www.youtube.com/embed/g4Ur0br2u8s" title="Computer mit Lernstick starten" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
{{</ rawhtml >}}

## Unterrichtseinheit

Die Unterrichtseinheit "Installieren" erklärt Schüler:innen den Installationsprozess einer Linux-Distribution.

Dieser ist gerade für Schüler:innen wahrscheinlich nicht ganz einfach, weshalb die Lehrperson Unterstützung bieten sollte.

*Die Installation kann auch von der Lehrperson vorgenommen werden, wenn die Zeit im Unterricht fehlt.*

*Mit dem Lernstick können gleich mehrere USB-Sticks auf einmal eingerichtet werden.*

