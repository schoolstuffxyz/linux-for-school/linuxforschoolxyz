---
title: "/mein-webserver"
menu:
    teacher:
        parent: "/einheiten"
files:
    - ["Lehrpersonenkommentar", "https://gitlab.com/schoolstuff-ch/linux-macht-schule/linux-macht-schule/-/releases/permalink/latest/downloads/release/lehrpersonenkommentar.pdf"]
    - ["Mein Webserver", "https://gitlab.com/schoolstuff-ch/linux-macht-schule/linux-macht-schule/-/releases/permalink/latest/downloads/release/mein-webserver.pdf"]
weight: 4
featured_image: '/images/webserver.jpg'
---

{{< toc >}}

## Webserver

Ein Webserver ist ein Computer, der mit einem Netzwerk verbunden ist und eine Webseite als Textdokument (HTML) zur Verfügung stellt.

Einen lokalen Webserver einzurichten ist nicht schwierig.
Es gibt unter Linux viele Programme, die es ermöglichen einen lokalen (oder auch nicht) Webserver einzurichten.

Diese Programme bedienen sich so ziemlich alle des Terminals.
Die notwendigen Befehle können jedoch einfach kopiert werden, was die Aufgabe erleichtert.

## Unterrichtseinheit

In diesem Kapitel lernen die Schüler:innen, eine eigene Webseite mit HTML zu schreiben und einen lokalen Webserver im Schulnetzwerk aufzusetzen.

Anfangs wird erklärt, wie die Sprache HTML funktioniert und wie eine einfach Webseite aufgebaut wird.

Diese Webseite können die Schüler:innen selber schreiben und dann in ihrem Browser ansehen.

Gegen Schluss der Einheit können die Schüler:innen ihre Webseite mit dem Programm "php" im lokalen Netzwerk veröffentlichen.

Zudem werden sie aufgefordert, ihre Webseite auszugestalten und sie einem bestimmten Thema zu widmen.

### HTML-Hilfe

Auf dieser Seite im [Schüler:innen-Bereich](/einheiten/weberver/) finden die Schüler:innen alles, das sie zum Schreiben einer Webseite benötigen.

Die Informationen stammen aus den folgenden Webseiten:

- [Fillster](http://fillster.com/htmlcodes/htmltags.html) ist eine englische Seite, die grundlegende HTML-Elemente erklärt.
- [Marquees](http://fillster.com/htmlcodes/htmltags.html): die Fillster-Seite erklärt hier, wie "Marquees" (bewegte Elemente) erstellt werden.
- [Gifcities](https://gifcities.org/) ist eine Seite, die das Internet nach animierten Gifs durchsucht.
