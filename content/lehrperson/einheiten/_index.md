---
title: "/einheiten"
menu: teacher
weight: 1
featured_image: '/images/default.jpg'
---

Die Unterrichtseinheiten dienen dazu, den Schüler:innen etwas über die jeweiligen Themenbereiche beizubringen.

Zu jeder Einheit gibt es auf dieser Website eine Webseite und ein dazugehöriges PDF, welches für die Schüler:innen ausgedruckt werden kann.
Zudem findet sich auf jeder Seite der Lehrpersonenkommentar, welcher sich auf alle Unterrichtseinheiten bezieht.

Die Einheiten wurden am Beispiel der Linux-Distribution "Lernstick" erstellt und enthalten Screenshot aus dessen Betriebssystem.
Sollten Sie sich für eine andere Linux-Distro entscheiden, können die Einheiten dennoch genau so durchgeführt werden.

