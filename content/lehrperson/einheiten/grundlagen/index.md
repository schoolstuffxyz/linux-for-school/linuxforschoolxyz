---
title: "/grundlagen"
menu:
    teacher:
        parent: "/einheiten"
files:
    - ["Lehrpersonenkommentar", "https://gitlab.com/schoolstuff-ch/linux-macht-schule/linux-macht-schule/-/releases/permalink/latest/downloads/release/lehrpersonenkommentar.pdf"]
    - ["Grundlagen", "https://gitlab.com/schoolstuff-ch/linux-macht-schule/linux-macht-schule/-/releases/permalink/latest/downloads/release/grundlagen.pdf"]
weight: 2
featured_image: '/images/grundlagen.jpg'
---

{{< toc >}}

Da sich Linux doch ein wenig von Windows und MacOS unterscheidet, gibt die Einheit "Grundlagen" den Schüler:innen einen ersten Einblick in die Arbeit mit Linux.

## Programme

### Programme installieren

Grundsätzlich werden die Programme unter Linux von einer Programm-Verwaltungs-Software ("apt" auf dem Lernstick und Edubuntu) installiert, geupdatet und deinstalliert.

Diese Programme werden eigentlich wie folgt über das Terminal (die Kommandozeile) bedient:

```bash
sudo apt install "firefox"
sudo apt remove "firefox"

```

Die meisten grossen Linux-Distributionen haben jedoch eigene grafische Programme, über die Programme ohne die Befehlszeile installiert werden können.
Unter [GNOME](/lehrperson/glossar#desktop-umgebung) (Lernstick EDU, Edubuntu) heisst dieses einfach "Software":

![Screenshot Software](/images/software-window.png)

Es gibt aber auch noch andere Möglichkeiten, unter Linux Programme zu installieren:

- [Flatpak](https://www.flatpak.org/) mit dem App Store [Flathub](https://flathub.org/)
- [Snap](https://snapcraft.io/about) mit dem [Snap Store](https://snapcraft.io/store)
- [AppImage](https://appimage.org/) mit dem inoffiziellen [AppImageHub](https://www.appimagehub.com/)
- [Wine](https://www.winehq.org/) (Schnittstelle für Windows-Programme)

### Alternative Software

Einige Programme, die unter Windows oder MacOS zur Standardausführung gehören, sind unter Linux nicht verfügbar.
Dazu gehören die Office Suite von Microsoft und die Creative-Suite von Adobe.
Deshalb müssen hier Alternativen verwendet werden, diese sind im Lernstick EDU bereits vorinstalliert.

Wenn Sie Alternativen zu Software suchen, bietet die Seite [alternativeto.net](https://alternativeto.net/) einen herausragenden Service mit Alternativen für jedes Betriebssystem.

#### LibreOffice (Win / MacOS: Microsoft Office)

[LibreOffice](https://libreoffice.org/) ist eine Office Suite zum Erstellen und Bearbeiten von Dokumenten.

- Writer (zur Bearbeitung von Textdokumenten)
- Impress (zur Erstellung von Präsentationen)
- Calc (für Tabellenkalkulationen)
- Draw (zum Zeichnen und Bearbeiten von u. a. PDFs)

![Screenshot LibreOffice](/images/libreoffice-window.png)

*Es gibt noch einige andere Office Suites, falls Ihnen diese nicht gefallen sollte.*

#### Dateien (Win: Explorer / MacOS: Finder)

Der Dateimanager "[Nautilus](https://apps.gnome.org/Nautilus/)" ist auf GNOME (Lernstick EDU, Edubuntu) der Standard und bietet alles, was zum Verwalten von Dokumenten gebraucht wird.

![Screenshot Nautilus](/images/nautilus-window.png)

*Es gibt noch viele andere Dateimanager, sollte Ihnen dieser nicht zusagen.*

#### Terminal (Win: PowerShell)

Das Terminal (oder die Kommando- / Befehlszeile) ist ein Programm mit dem über Text-Befehle so ziemlich alles auf dem Computer angesteuert und bearbeitet werden kann.

Die Standardmässige "Sprache" (Shell) -- die hier eingestellt ist -- ist meistens [Bash](https://www.gnu.org/software/bash/) oder manchmal [Zsh](https://zsh.sourceforge.io/).
Diese Sprachen folgen dem selben Standard und unterscheiden sich nur wenig. Empfohlen wird für Einsteiger die Bash Shell.

Für die Schüler:innen bietet sich über das Terminal die Möglichkeit, Programmieren zu lernen oder bspw. einen eigenen Webserver einzurichten.

![Screenshot Terminal](/images/terminal-window.png)

*Auf GNOME ist [Console](https://apps.gnome.org/Console/) der Standard Terminal Emulator. Auch dieser kann natürlich durch einen anderen ersetzt werden.*

### Vorhandene Software

Sehr viele Software für den Unterricht ist auch auf Linux verfügbar.
Dazu zählen in erster Linie alle Programme, die als WebApp verfügbar sind, aber auch:

#### Browser

- [Firefox](https://www.mozilla.org/en-US/firefox/) (meist der Standard)
- [Brave](https://brave.com/) (ein Browser mit integriertem Ad-Blocker)
- [Chrome](https://www.google.com/chrome/) (wird hier eher nicht empfohlen)

#### Medien und Informatik

- [Scratch](https://scratch.mit.edu/) (Web & Desktop)
- [Code it](https://code-it-studio.de/) (Web)

#### Spiele

- [Minecraft](https://www.minecraft.net/en-us) ([spiegel.de](https://www.spiegel.de/netzwelt/games/minecraft-so-kann-man-mit-dem-kultspiel-programmieren-lernen-a-1165498.html) programmieren lernen mit Minecraft)

## Unterrichtseinheit

Die Unterrichtseinheit "Grundlagen" bietet den Schüler:innen nur einen kleinen Einblick in das System und die Programme.

Da auf dem Linux-System eigentlich alle MuI-Aufgaben bewältigt werden können, kann die Lehrperson ganz normal die Aufträge erteilen
und es müssen nur zum Teil andere Programme verwendet werden.
