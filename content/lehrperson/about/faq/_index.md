---
title: "/faq"
menu:
    teacher:
        parent: "/about"
weight: 3
featured_image: '/images/default.jpg'
---

### 1. Was sollen diese komischen Titel?

Die Wahl der Titel-Darstellung geschah in Anlehnung an die Linux-Verzeichnisstruktur.

Die Tilde (~) markiert in der Shell das Home-Verzeichnis des jeweiligen Benutzers und hier auf der Website jeweils die Startseite für Lehrpersonen bzw. Schüler:innen.
Alle weiteren Seiten sitzen unterhalb dieser Hauptseite und sind deshalb als Verzeichnisse innerhalb des Home-Verzeichnisses dargestellt.

Die einzelnen Inhaltsseiten -- die mit einem Punkt beginnen -- markieren mit dem Punkt das jeweils aktuelle Verzeichnis und nach dem Schrägstrich wiederum das aktuelle Dokument.

