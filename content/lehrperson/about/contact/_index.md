---
title: "/kontakt"
menu: 
    teacher:
        parent: "/about"
weight: 1
featured_image: '/images/default.jpg'
---

Möchtest du mich kontaktieren, so bin ich unter dieser [E-Mail](mailto:contact@azureorange.xyz) erreichbar.

