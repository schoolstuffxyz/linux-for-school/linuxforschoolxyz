---
title: "/mitmachen"
menu: 
    teacher:
        parent: "/about"
weight: 2
featured_image: '/images/default.jpg'
---

Wenn du Anregungen zum Projekt hast oder Materialien zu Verfügung stellen willst, erstelle einfach einen **Merge Request** oder ein **Issue** in einem der [Gitlab Repositories](https://gitlab.com/schoolstuff-ch/linux-macht-schule) des Projekts.

