---
title: "/about"
date: 2022-07-04T11:59:08+02:00
menu: main
weight: 10
featured_image: '/images/default.jpg'
no_list: true
---

Ich bin seit einigen Jahren Linux-Nutzer und sehe in der Verwendung von Linux als Schul-Betriebssystem viele Vorteile.

Das Projekt "Linux macht Schule" ist im Rahmen [meiner Bachelorarbeit](/thesis) an der PHSG entstanden.

Das Ziel ist, den Zugang zu Linux einfacher zu machen und evtl. einige coole Unterrichtsideen zu publizieren.
Im Rahmen der Bachelorarbeit sind bereits einige solche entstanden, diese werde ich jedoch noch verbessern und weiterentwickeln.
