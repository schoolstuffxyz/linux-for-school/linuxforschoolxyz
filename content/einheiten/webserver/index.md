---
title: "/mein-webserver"
menu:
    main:
        parent: "/einheiten"
files:
    - ["Mein Webserver", "https://gitlab.com/schoolstuff-ch/linux-macht-schule/linux-macht-schule/-/releases/permalink/latest/downloads/release/mein-webserver.pdf"]
weight: 4
featured_image: '/images/webserver.jpg'
---

## Herzlich willkommen!

In diesem Kapitel wirst du lernen, wie du deine eigene Webseite erstellen kannst.
Wir werden mit HTML (der Sprache des Internets) arbeiten und sogar einen einfachen Webserver mit dem Programm PHP aufsetzen.

Lass uns zusammen durchstarten!

## HTML-Hilfe

Hier findest du einige Seiten, die dir beim Schreiben in der HTML-Sprache helfen können.

### Grundlegendes

In dieser Tabelle findest du die grundlegensten HTML-Elemente zum nachschlagen:

{{< rawhtml >}}
<details class="toc">
<summary class="toc">Tabelle: Grundlegende HTML-Elemente</summary>
<table>
  <tr>
    <td>&lt;a&gt;</td><td>Das Anker-Element generiert einen &lt;a href="/einheiten"&gt;<a href="/einheiten">Link</a>&lt;/a&gt; zu einer anderen Seite oder URL.</td>
  </tr>
  <tr>
    <td>&lt;b&gt;</td><td>Das Bold-Element macht einen Teil des Textes &lt;b&gt;<b>fett</b>&lt;/b&gt;.</td>
  </tr>
  <tr>
    <td>&lt;bgsound&gt;</td>
    <td>Das Bgsound-Element fügt deiner Seite Hintergrundsmusik hinzu (plaziere es im Kopf der Seite):
    &lt;head&gt;
      &lt;bgsound src="musik.wav" loop="3"&gt;
    &lt;/head&gt;</td>
  </tr>
  <tr>
    <td>&lt;big&gt;</td><td>Das Big-Element macht deinen Text &lt;big&gt;<big>grösser</big>&lt;/big&gt;.</td>
  </tr>
  <tr>
    <td>&lt;body&gt;</td><td>&lt;body&gt;Das Body-Element enthält den ganzen Inhalt (Körper) deiner Webseite.&lt;/body&gt;</td>
  </tr>
  <tr>
    <td>&lt;br&gt;</td><td>Das Br-Element fügt einen&lt;br&gt;<br>Zeilenumbruch ein.</td>
  </tr>
  <tr>
    <td>&lt;button&gt;</td><td>Das Button-Element fügt einen &lt;button&gt;<button>Druckknopf</button>&lt;/button&gt; ein.</td>
  </tr>
  <tr>
    <td>&lt;center&gt;</td><td><center>&lt;center&gt;Das Center-Element zentriert den Text.&lt;/center&gt;</center></td>
  </tr>
  <tr>
    <td>&lt;div&gt;</td>
    <td>Das Div-Element enthält einzelne Abschnitte in deinem Dokument:
    &lt;div&gt;
      &lt;h1&gt;Das ist ein neuer Abschnitt.&lt;/h1&gt;
      &lt;p&gt;Mit einem Paragraphen.&lt;/p&gt;
    &lt;/div&gt;
    </td>
  </tr>
  <tr>
    <td>&lt;embed&gt;</td><td>Das Embed-Element signalisiert dem Browser ein Medium (Video / Audio) in die Seite einzubetten.
    &lt;embed src="video.mp4"&gt;</td>
  </tr>
  <tr>
    <td>&lt;h1&gt; - &lt;h6&gt;</td><td>Die Header-Elemente formatieren deinen Text zu einer Überschrift in verschiedenen Grössen.<h6 style="padding:0; margin:0;">&lt;h6&gt;Das ist zum Beispiel die kleinste Überschrift&lt;/h6&gt;</h6></td>
  </tr>
  <tr>
    <td>&lt;head&gt;</td><td>Das Head-Element beinhaltet den Kopf deiner Seite und enthält generelle Informationen über das Dokument: 
Seitentitel, Metadaten, Skripte und vieles mehr, über das du dir jetzt nicht zu viele Gedanken machen musst.</td>
  </tr>
  <tr>
    <td>&lt;hr&gt;</td><td>Das HR-Element fügt eine horizontale Linie ein.<hr></td>
  </tr>
  <tr>
    <td>&lt;html&gt;</td><td>Das Html-Element enthält alle anderen HTML-Elemente, also auch Kopf und Körper der Seite.</td>
  </tr>
  <tr>
    <td>&lt;img&gt;</td><td>Das Img-Element fügt ein Bild in die Seite ein:
&lt;img src="tux.gif" /&gt;<img src="/images/tux.gif" /></td>
  </tr>
  <tr>
    <td>&lt;i&gt;</td><td>Das Italic-Element formatiert deinen Text &lt;i&gt;<i>kursiv</i>&lt;/i&gt;.</td>
  </tr>
  <tr>
    <td>&lt;li&gt;</td><td>Das List-Element definiert eine Liste mit sortierten oder unsortierten Elementen.
&lt;ol&gt;<ol><li>&lt;li&gt;erstes&lt;/li&gt;</li><li>&lt;li&gt;zweites&lt;/li&gt;</li><li>&lt;li&gt;drittes&lt;/li&gt;</li></ol>&lt;/ol&gt;
&lt;ul&gt;<ul><li>&lt;li&gt;rot&lt;/li&gt;</li><li>&lt;li&gt;gelb&lt;/li&gt;</li><li>&lt;li&gt;blau&lt;/li&gt;</li></ul>&lt;/ul&gt;
    </td>
  </tr>
  <tr>
    <td>&lt;ol&gt;</td><td>Das OL-Element definiert eine sortierte Liste.</td>
  </tr>
  <tr>
    <td>&lt;p&gt;</td><td>Das P-Element definiert einen neuen Paragraphen<p>&lt;p&gt;auf einer neuen Zeile.&lt;/p&gt;</p></td>
  </tr>
  <tr>
    <td>&lt;s&gt;</td><td>Das S-Element &lt;s&gt;<s>streicht Text durch</s>&lt;/s&gt;.</td>
  </tr>
  <tr>
    <td>&lt;u&gt;</td><td>Das U-Element &lt;u&gt;<u>unterstreicht deinen Text</u>&lt;/u&gt;.</td>
  </tr>
  <tr>
    <td>&lt;ul&gt;</td><td>Das UL-Element definiert eine unsortierte Liste.</td>
  </tr>
</table>

Diese Inhalte sind von der Seite <a href="http://fillster.com/htmlcodes/htmltags.html">Fillster.com</a>, auf welcher auch noch alle weiteren Elemente beschrieben sind.
</details>
{{< /rawhtml >}}

### Lustiges

Mit HTML kannst du die Dinge auch in Bewegung setzen.

{{< rawhtml >}}
<details class="toc">
<summary class="toc">Tabelle: Bewegte HTML-Elemente</summary>
<table>
  <tr>
    <td>&lt;marquee&gt;</td><td><marquee>&lt;marquee&gt;Dieser Text bewegt sich von rechts nach links.&lt;/marquee&gt;</marquee></td>
  </tr>
  <tr>
    <td>&lt;marquee behavior="slide"&gt;</td><td><marquee behavior="slide">&lt;marquee behaviour="slide"&gt;Dieser Text rutsch von rechts herein.&lt;/marquee&gt;</marquee></td>
  </tr>
  <tr>
    <td>&lt;marquee behavior="alternate"&gt;</td><td><marquee behavior="alternate">&lt;marquee behavior="alternate"&gt;Dieser Text wechselt die Richtung.&lt;/marquee&gt;</marquee></td>
  </tr>
  <tr>
    <td>&lt;marquee direction="right"&gt;</td><td><marquee direction="right">&lt;marquee direction="right"&gt;Dieser Text bewegt sich nach rechts.&lt;/marquee&gt;</marquee></td>
  </tr>
  <tr>
    <td>&lt;marquee direction="up"&gt;</td><td><marquee direction="up">&lt;marquee direction="up"&gt;Dieser Text bewegt sich nach oben.&lt;/marquee&gt;</marquee></td>
  </tr>
  <tr>
    <td>&lt;marquee direction="down"&gt;</td><td><marquee direction="down">&lt;marquee direction="down"&gt;Dieser Text bewegt sich nach unten.&lt;/marquee&gt;</marquee></td>
  </tr>
  <tr>
    <td>&lt;marquee loop="5"&gt;</td><td><marquee loop="5">&lt;marquee loop="5"&gt;Dieser Text kommt nur fünf mal.&lt;/marquee&gt;</marquee></td>
  </tr>
  <tr>
    <td>&lt;marquee scrollamount="20"&gt;</td><td><marquee scrollamount="20">&lt;marquee scrollamount="20"&gt;Dieser Text bewegt sich schneller.&lt;/marquee&gt;</marquee></td>
  </tr>
  <tr>
    <td>&lt;marquee scrolldelay="500"&gt;</td><td><marquee scrolldelay="500">&lt;marquee scrolldelay="500"&gt;Dieser Text bewegt sich verzögert.&lt;/marquee&gt;</marquee></td>
  </tr>
  <tr>
    <td>&lt;marquee bgcolor="#ff8800"&gt;</td><td><marquee bgcolor="#ff8800">&lt;marquee bgcolor="#ff8800"&gt;Dieser Text hat eine andere Hintergrundfarbe.&lt;/marquee&gt;</marquee></td>
  </tr>
</table>
<table>
  <tr>
    <td><center>Du kannst auch verschidene Marquees miteinander kombinieren.</center></td>
  </tr>
  <tr>
    <td><marquee behavior="alternate"><marquee width="200">Probiere das doch einfach einmal aus.</marquee></marquee></td>
  </tr>
  <tr>
    <td><center>Oder Bilder in den Marquee einfügen.</center></td>
  </tr>
  <tr>
    <td><marquee scrolldelay="50" style="margin-bottom:-1em;"><img src="/images/stick-figure.gif" style="height:3em;"></marquee></td>
  </tr>
  <tr>
    <td>Solche sich bewegenden Bilder (wie das des Männchens oben) nennt man Gifs.
Früher war das Internet voll mit diesen Bildern, doch vielleicht gefallen sie dir und du erweckst sie zu neuem Leben.
Auf dieser Seite findest du viele Gifs, die irgendwo im Internet zuhause waren: <a href="https://gifcities.org">GifCities.org</a></td>
  </tr>
</table>
</details>
{{< /rawhtml >}}

