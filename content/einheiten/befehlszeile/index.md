---
title: "/die-befehlszeile"
menu:
    main:
        parent: "/einheiten"
files:
    - ["die Befehlszeile", "https://gitlab.com/schoolstuff-ch/linux-macht-schule/linux-macht-schule/-/releases/permalink/latest/downloads/release/die-befehlszeile.pdf"]
    - ["Bashcrawl", "https://gitlab.com/schoolstuff-ch/linux-macht-schule/bashcrawl/-/releases"]
weight: 3
featured_image: '/images/terminal.jpg'
---

## Herzlich willkommen!

Die Befehlszeile (oder das Terminal) ist wie die Sprache, in der du deinem Computer sagen kannst, was er tun soll.
Lass uns gemeinsam die Grundlagen lernen und mächtige Dinge tun!

Mit der Pdf-Anleitung "die Befehlszeile" kannst du die Grundlagen der Befehlszeile lernen.

### Was ist die Befehlszeile?

Die Befehlszeile ist ein Ort, an dem du direkt mit deinem Computer sprechen kannst.
Es ist wie ein geheimer Club, in dem du coole Dinge tun kannst.
Sie ist sehr mächtig und mit ihr kannst du viele Sachen machen, die du normal so nicht machen kannst.

### Spiele

Obwohl du in der Befehlszeile nur mit Text-Befehlen arbeiten kannst, gibt es trotzdem Spiele die du darin spielen kannst.
Ein Abenteuerspiel, mit dem du auch gleich die Befehlszeile entdecken kannst, heisst Bashcrawl.
Wenn du ein Anfänger bist, empfehle ich dir als erstes dieses Spiel zu spielen.

Du kannst es auf der hier verlinkten Seite herunterladen.

### Wie funktioniert die Befehlszeile?

![befehlszeile](/images/terminal-narrow.gif)

Das ist der Bildschirm, auf dem wir arbeiten werden.
Hier gibst du Befehle ein und dein Computer antwortet.

Damit du einen Befehl absenden kannst, musst du nur die \<Enter>-Taste drücken.

Öffne ein Terminal und lass uns herausfinden, was wir ihm sagen können!

- 'ls' - Zeige die Dateien in einem Ordner an.
- 'cd' - Wechsle in einen anderen Ordner.
- 'pwd' - Zeige deinen aktuellen Pfad / Ort an.
- 'mkdir' - Erstelle einen neuen Ordner.
- 'cp' - Kopiere Dateien von einem Ort zum anderen.

