---
title: "/einheiten"
menu: main
weight: 1
featured_image: '/images/default.jpg'
---

Es gibt zahlreiche Linux-Distributionen (Varianten) und darunter auch einige, die speziell für den schulischen Einsatz erstellt wurden.

Davon zu empfehlen sind der [Lernstick](https://lernstick.ch) aus der Schweiz und das etwas bekanntere [Edubuntu](https://edubuntu.org).

Sollten dir diese nicht zusagen, so findest du auf [DistroWatch.com](https://distrowatch.com) genau die richtige Distro.
