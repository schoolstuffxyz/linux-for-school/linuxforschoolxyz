---
title: "/installieren"
menu:
    main:
        parent: "/einheiten"
files:
    - ["Linux installieren", "https://gitlab.com/schoolstuff-ch/linux-macht-schule/linux-macht-schule/-/releases/permalink/latest/downloads/release/linux-installieren.pdf"]
    - ["Lernstick EDU", "https://releases.lernstick.ch/lernstick_debian12_latest.iso"]
    - ["Edubuntu", "https://cdimages.ubuntu.com/edubuntu/releases/23.10/release/edubuntu-23.10-desktop-amd64.iso"]
weight: 1
featured_image: '/images/installieren.jpg'
---

## Herzlich willkommen! 

Hier erfährst du, wie du Linux auf deinem Computer installieren kannst.
Das ist der erste Schritt, um ein Entdecker in der Welt der Computer zu werden!

Linux ist wie ein Abenteuerland in deinem Computer.
Es ist kostenlos, sicher und macht Spaß!
Wenn du Linux installierst, kannst du deinen Computer auf eine neue und coole Weise kennenlernen.

### Vorbereitung für die Installation

Bevor wir loslegen, schauen wir uns an, was du brauchst.
Du benötigst einen Computer, einen USB-Stick, ein Linux-Abbild und deine Abenteuerlust!

Der USB-Stick sollte leer sein, weil alle Daten auf dem Stick beim Installieren verloren gehen.

Das Linux-Abbild (iso) kannst du auf den jeweiligen Webseiten herunterladen.
Auf dieser Seite findest du die Abbilder vom Lernstick und Edubuntu.

Du kannst auch eine erwachsene Person um Hilfe bitten.

### Schritt-für-Schritt-Anleitung

Wir werden die Installation in kleinen Schritten machen.
 
Wenn das dein erstes Mal ist, dann bitte doch deine Lehrperson oder deine Eltern um Hilfe, damit auch nichts schief geht.

Die Anleitung kannst du hier als Pdf herunterladen.

Lass uns gemeinsam loslegen und Linux auf deinem Computer installieren.

