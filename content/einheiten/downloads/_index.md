---
title: "/downloads"
featured_image: '/images/literature.jpg'
menu:
    main:
        parent: "/einheiten"
---

{{< rawhtml >}}
<h4 id="downloads">
    Einheiten:
</h4>
<span class="download-link">
    <a href="https://gitlab.com/schoolstuff-ch/linux-macht-schule/linux-macht-schule/-/releases/permalink/latest/downloads/release/lehrpersonenkommentar.pdf" class="icon-h" style="width:100%">
        <img src="/icons/pdf.svg" alt-text="Pdf:"></img>
        <span>Lehrpersonenkommentar</span>
    </a>
</span>

<span class="download-link">
    <a href="http://gitlab.com/schoolstuff-ch/linux-macht-schule/linux-macht-schule/-/releases/permalink/latest/downloads/release/grundlagen.pdf" class="icon-h" style="width:100%">
        <img src="/icons/pdf.svg" alt-text="Pdf:"></img>
        <span>Grundlagen</span>
    </a>
</span>

<span class="download-link">
    <a href="http://gitlab.com/schoolstuff-ch/linux-macht-schule/linux-macht-schule/-/releases/permalink/latest/downloads/release/die-befehlszeile.pdf" class="icon-h" style="width:100%">
        <img src="/icons/pdf.svg" alt-text="Pdf:"></img>
        <span>Die Befehlszeile</span>
    </a>
</span>

<span class="download-link">
    <a href="http://gitlab.com/schoolstuff-ch/linux-macht-schule/linux-macht-schule/-/releases/permalink/latest/downloads/release/mein-webserver.pdf" class="icon-h" style="width:100%">
        <img src="/icons/pdf.svg" alt-text="Pdf:"></img>
        <span>Mein Webserver</span>
    </a>
</span>
{{</ rawhtml >}}

{{< rawhtml >}}
<h4 id="downloads">
    Programme:
</h4>
<span class="download-link">
    <a href="https://gitlab.com/schoolstuff-ch/linux-macht-schule/bashcrawl/-/releases" class="icon-h" style="width:100%">
        <img src="/icons/releases.svg" alt-text="Pdf:"></img>
        <span>Bashcrawl</span>
    </a>
</span>
{{</ rawhtml >}}

{{< rawhtml >}}
<h4 id="downloads">
    Linux-Distributionen:
</h4>
<span class="download-link">
    <a href="https://releases.lernstick.ch/lernstick_debian12_latest.iso" class="icon-h" style="width:100%">
        <img src="/icons/iso.svg" alt-text="Pdf:"></img>
        <span>Lernstick EDU</span>
    </a>
</span>

<span class="download-link">
    <a href="https://cdimages.ubuntu.com/edubuntu/releases/23.10/release/edubuntu-23.10-desktop-amd64.iso" class="icon-h" style="width:100%">
        <img src="/icons/iso.svg" alt-text="Pdf:"></img>
        <span>Edubuntu 23.10</span>
    </a>
</span>
{{</ rawhtml >}}
