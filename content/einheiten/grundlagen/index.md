---
title: "/grundlagen"
menu:
    main:
        parent: "/einheiten"
files:
    - ["Grundlagen", "https://gitlab.com/schoolstuff-ch/linux-macht-schule/linux-macht-schule/-/releases/permalink/latest/downloads/release/grundlagen.pdf"]
weight: 2
featured_image: '/images/grundlagen.jpg'
---

## Willkommen in der Welt von Linux

Hier beginnt deine Reise zu den Grundlagen von Linux.
Du wirst verstehen, was Linux ist und wie es funktioniert.
Lass uns loslegen und die Bausteine der Computerwelt entdecken!

### Was ist Linux?

Linux ist ein Betriebssystem, das wie das Gehirn deines Computers arbeitet.
Davon gibt es viele verschiedene Arten.
Dein Smartphone funktioniert wahrscheinlich auch mit einer Art Linux, die Android heisst.

### Die Linux-Oberfläche

Die Linux-Oberfläche sieht vielleicht anders aus als das, was du gewohnt bist.
Auch hier gibt es viele verschiedene solche Oberflächen.
Der Lernstick zum Beispiel hat eine, die GNOME genannt wird.

Mit dem Pdf "Grundlagen" kannst du die Symbole und Optionen erkunden, damit du dich wie zu Hause fühlst.

### Dateien und Ordner

Auf einem Computer gibt es viele Dateien und Ordner, genau wie in einer Schultasche.
In Linux kann alles als eine Datei betrachtet werden.
Auch ein Ordner oder ein Laufwerk -- wie z. B. ein USB-Stick -- werden als Datei angezeigt.

### Benutzerkonten

Jeder hat auf einem Computer sein eigenes Konto.
Das Konto des Linux-Pinguins könnte zum Beispiel "tux" heissen, deinem eigenen darfst du natürlich selber einen Namen geben.

Ein ganz wichtiges Konto ist das Administrator - oder "root" - Konto.
Dieses brauchst du, wenn du zum Beispiel Programme installieren willst.



