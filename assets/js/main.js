document.addEventListener('DOMContentLoaded', function() {
    if (!localStorage.getItem('pageLoaded')) {
        localStorage.setItem('pageLoaded', true);
    }
    if (!localStorage.getItem('lmsUser')) {
        var usermenu = document.getElementById('userMenu');
        var menubutton = document.getElementById('userMenuButton');
        usermenu.style.display = 'block';
        menubutton.style.display = 'none';
    } else {
    if (lmsUser == 'teacher') {
            teacher();
        } else {
            pupil();
        }
    }
});

function change_visibility() {
    var usermenu = document.getElementById('userMenu');
    var menubutton = document.getElementById('userMenuButton');
    var displaySetting = usermenu.style.display;
    
    if (displaySetting == 'none') {
        usermenu.style.display = 'block';
        menubutton.style.display = 'none';
    } else {
        usermenu.style.display = 'none';
        menubutton.style.display = 'block';
    }
}

function teacher() {
        if (window.location.pathname == '/') {
            window.location.pathname = '/lehrperson';
        } else {
            var currentUrl = window.location.pathname;
            if (currentUrl.includes("lehrperson/") == false) {
                window.location.pathname = "/lehrperson" + currentUrl;
            }
        }
}

function pupil() {
        var currentUrl = window.location.pathname;
        window.location.pathname = currentUrl.replace("lehrperson/", "");
}

function handleButtonClick(lmsUser) {
    // Auswahl im Local Storage speichern
    localStorage.setItem('lmsUser', lmsUser);

    if (lmsUser == 'teacher') {
        teacher();
        change_visibility();
    } else {
        pupil();
        change_visibility();
    }
}

// Event Listener für Peter-Button
document.getElementById('teacherButton').addEventListener('click', function() {
    handleButtonClick('teacher');
});

// Event Listener für Maria-Button
document.getElementById('pupilButton').addEventListener('click', function() {
    handleButtonClick('pupil');
});

// Kein separater Event Listener für das DOMContentLoaded-Event erforderlich,
// da die Markierung 'pageLoaded' bereits verwendet wird.

